import snakeoil
import numpy as np
import random
import copy
import generate_xml
import os
import sys
from collections.abc import Iterable
from collections import deque
from neural_network import QNetwork
from scipy.special import softmax

stuckTimeLimit = 5000
stuckTime = 0
minDist = 0.2
lastDist = 0
training = 0
restart = False

interestingFeatures = ('angle','fuel','gear','racePos','rpm','speedX','speedY','track','trackPos','wheelSpinVel','z','focus')

obs_size = 37
lastObs = np.zeros((1, obs_size))
my_step = 0

max_obs = np.ones((1, obs_size))
min_obs = np.zeros((1, obs_size))
discount = 0.99
qNetwork = QNetwork(obs_size, 80, 80, 9, 0.0001)
targetNetwork = copy.deepcopy(qNetwork)

lastInd = 0

rb_observation = deque()
rb_action = deque()
rb_reward = deque()
rb_nxt_observation = deque()

def drive(c):
    global stuckTime, lastDist, lastObs, restart, max_obs, min_obs, my_step
    global lastInd, T, maxDist
    global training, step, targetNetwork
    global rb_observation, rb_action, rb_reward, rb_nxt_observation
    S, R= c.S.d, c.R.d

    my_step += 1
    if my_step == 4000000:
        os.system('pkill torcs')
        sys.exit()

    if my_step % 20000 == 0: 
        targetNetwork = copy.deepcopy(qNetwork)

    ex = []
    for z in interestingFeatures:
        if isinstance(S[z], Iterable): ex.extend(S[z])
        else: ex.append(S[z])
    np_ex = np.reshape(np.array(ex), (1, obs_size))

    min_obs = np.minimum(min_obs, np_ex)
    max_obs = np.maximum(max_obs, np_ex)
    np_ex = ((np_ex - min_obs) / (max_obs - min_obs)) - 0.5

    results = qNetwork.result(np_ex)

    speed = (S['speedX'] * np.cos(S['angle']) - S['speedX'] * np.sin(S['angle']))
    reward = (speed - (S['speedX'] * abs(S['trackPos'])))/200


    if S['damage'] > 0:
        reward = -10
        np_ex = None

    update_val = reward
    if np_ex is not None: update_val = reward + discount * targetNetwork.result(np_ex)[np.argmax(results)]
    
    ind = np.argmax(results)
    if my_step < 3000000: epsilon = 1/np.log(my_step + 3)
    else: epsilon = (0.30 * (1 - ((my_step - 3000000)/1000000))) + 0.05
    if np.random.binomial(1, epsilon) == 1:
        ind = random.SystemRandom().choice(range(9))

    R['brake'] = 0
    R['accel'] = 0
    R['steer'] = 0

    if ind == 0: 
        R['accel'] = 1
    elif ind == 1: 
        R['brake'] = 1
    elif ind == 3: 
        R['steer'] = -1
    elif ind == 4: 
        R['steer'] = 1 
    elif ind == 5:
        R['steer'] = 0.3
    elif ind == 6:
        R['steer'] = -0.3
    elif ind == 7:
        R['accel'] = 0.3
    elif ind == 8:
        R['brake'] = 0.3
       
    print(S['distFromStart'], reward, my_step, S['trackPos'])

    if np_ex is not None: 
        rb_observation.append(lastObs) 
        rb_action.append(lastInd)
        rb_reward.append(reward)
        rb_nxt_observation.append(np_ex)

    else :
        rb_observation.append(lastObs) 
        rb_action.append(lastInd)
        rb_reward.append(reward)
        rb_nxt_observation.append(np.zeros((1, 37)))

    if len(rb_reward) > 250000:
        rb_observation.popleft()
        rb_action.popleft()
        rb_reward.popleft()
        rb_nxt_observation.popleft()

    if len(rb_reward) > 100:
        batch_size = 64
        ex_to_update = [int(random.SystemRandom().random() * len(rb_reward)) for _ in range(batch_size)]

        observations = np.squeeze(np.array([rb_observation[i] for i in ex_to_update]))
        actions = np.squeeze(np.array([rb_action[i] for i in ex_to_update]))
        rewards = np.squeeze(np.array([rb_reward[i] for i in ex_to_update]))
        next_observations = np.squeeze(np.array([rb_nxt_observation[i] for i in ex_to_update]))

        net_output = qNetwork.result(next_observations)
        t_ind = np.argmax(net_output, axis = 0)
        t_update_values = rewards + (discount * targetNetwork.result(next_observations))[t_ind, np.arange(batch_size)]
        np.putmask(t_update_values, rewards == -10, -10)
        qNetwork.update(observations, t_update_values, actions)


    if S['rpm'] < 500 * (S['gear'] + 6):
        R['gear'] = max(S['gear'] - 1, 1)
    if S['rpm'] > 9000:
        R['gear'] = S['gear'] + 1


    lastDist = S['distFromStart'] 
    lastObs = np_ex
    lastInd = ind

    if S['distFromStart'] - lastDist < minDist : stuckTime+=1
    else : stuckTime = 0
    if stuckTime >= stuckTimeLimit or S['damage'] > 0:
        R['meta'] = 1
        stuckTime = 0
        lastObs = np.zeros(obs_size)
        restart = True
    return


if __name__ == "__main__":

    while True:

        '''
        if training == 0: 
            qNetwork.load()
            min_obs = np.load('storage/min_obs.npy')
            max_obs = np.load('storage/max_obs.npy')
            targetNetwork = copy.deepcopy(qNetwork)
        '''


        qNetwork.save()
        np.save('storage/min_obs', min_obs)
        np.save('storage/max_obs', max_obs)
        training += 1
        restart = False
        lastObs = np.zeros((1, obs_size))

        initial_distance = random.SystemRandom().choice(np.arange(3000))
        initial_speed = 0
        track_name = random.SystemRandom().choice(('g-track-2', 'e-track-3', 'wheel-1', 'eroad', 'street-1'))

        print('initial distance:', initial_distance)
        print('track:', track_name)
        generate_xml.generate_map(3001, '/tmp/map.xml', initial_distance, initial_speed, track_name)
        os.system('torcs -r /tmp/map.xml -nofuel -nodamage -nolaptime -t 100000 &')


        C= snakeoil.Client()
        for step in range(C.maxSteps,0,-1):
            C.get_servers_input()
            drive(C)
            C.respond_to_server()

            if restart == True:
                os.system('pkill torcs')
                break

        C.shutdown()
