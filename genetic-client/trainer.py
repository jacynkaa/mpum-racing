import client
import os
import snakeoil
import functools
import math


def takeFirst(elem):
    return elem[0]


def admin():
    return os.getuid() == 0


def train(population, steps, gen):
    l = []
    for d in population:
        os.system('torcs -r  /home/jacek/Desktop/GIT/mpum-racing/genetic-client/practice.xml -t 100000 &')
        C = snakeoil.Client()

        for step in range(steps, 0, -1):
            C.get_servers_input()
            d.drive(C)
            C.respond_to_server()
            # if d.kraksa(C):  # w przypadku kraksy breakuje
            #     break

        l.append((d.getFitness(C), d))
        C.shutdown()
        os.system('pkill torcs')

    l.sort(key=lambda x: -x[0])
    print(l)
    l[0][1].Save('generacja' + str(gen))

    res = []

    r = int(math.sqrt(len(l)) + .2)

    for i in range(0, r):
        for j in range(0, r):
            res.append(l[i][1].makeChild(l[j][1]))

    return res


if __name__ == "__main__":

    if not admin():
        print("brak uprawnien")
        exit(-1)

    size = 100
    population = [client.driver() for i in range(0, size)]
    for generation in range(0, 20):
        print('generation'+str(generation))
        population = train(population, 5000, generation)

    exit(0)

    os.system('torcs -nofuel -nodamage -nolaptime &')
    C = snakeoil.Client()

    for step in range(1000, 0, -1):
        C.get_servers_input()
        d.drive(C)
        C.respond_to_server()
