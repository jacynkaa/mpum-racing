import client
import os
import snakeoil
import functools
import math
from concurrent.futures import ThreadPoolExecutor
import concurrent
import threading
import subprocess


def takeFirst(elem):
    return elem[0]


def admin():
    return os.getuid() == 0


def multi(d, x, steps):
    p = subprocess.Popen(["torcs", "-r",
                          "/home/jacek/Desktop/GIT/mpum-racing/genetic-tribe/practice%d.xml" % (x), "-t",
                          "100000"])
    pid = p.pid
    # return 22 + x

    C = snakeoil.Client(p=(3000 + x))
    for step in range(steps, 0, -1):
        C.get_servers_input()
        d.drive(C)
        C.respond_to_server()
        if d.kraksa(C):  # w przypadku kraksy breakuje
            break
    res = d.getFitness(C)
    C.shutdown()
    return res


def getNew(l, j, gen):
    l.sort(key=lambda x: -x[0])
    print(l)
    l[0][1].Save('gen/' + str(gen) + '_' + str(j))

    res = []

    r = int(math.sqrt(len(l)) + .2)

    for i in range(0, r):
        for j in range(0, r):
            if i != j:
                res.append(l[i][1].makeChild(l[j][1]))
            else:
                d = client.driver()
                d.N = l[i][1].N
                res.append(d)
    return res


def train(tribes, steps, gen):
    os.system('pkill torcs')
    l = [[] for i in range(0, len(tribes))]
    executor = ThreadPoolExecutor()

    for i in range(0, len(tribes[0])):
        F = []
        for j in range(0, len(tribes)):
            F.append(executor.submit(multi, tribes[j][i], j + 1, steps))
        concurrent.futures.wait(F)
        for j in range(0, len(tribes)):
            l[j].append((F[j].result(), tribes[j][i]))
        os.system('pkill torcs')

    for j in range(0, len(tribes)):
        tribes[j] = getNew(l[j], j, gen)

    for j in range(0, len(tribes)):
        tribes[j].append(tribes[(j+1)%len(tribes)][0])


    return tribes


if __name__ == "__main__":

    if not admin():
        print("brak uprawnien")
        exit(-1)

    size = 100
    tribes = [[client.driver() for i in range(0, size)] for j in range(0, 5)]

    for generation in range(0, 100):
        print('generation' + str(generation))
        population = train(tribes, 5000, generation)
    exit(0)

    os.system('torcs -nofuel -nodamage -nolaptime &')
    C = snakeoil.Client()

    for step in range(1000, 0, -1):
        C.get_servers_input()
        d.drive(C)
        C.respond_to_server()
