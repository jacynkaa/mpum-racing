# !/usr/bin/python
import snakeoil
import numpy as np
import pickle
import math
import copy
import os
import sys


class Siec:
    zakres = 1

    def __init__(self, input, hidden, output=2):
        self.input = input
        self.hidden = hidden
        self.output = output
        zakres = 1
        self.w1 = np.random.uniform(-zakres, zakres, (input, hidden))
        self.b1 = np.random.uniform(-zakres, zakres, (hidden))
        self.w2 = np.random.uniform(-zakres, zakres, (hidden, output))
        self.b2 = np.random.uniform(-zakres, zakres, (output))

    def offspring(self, other):
        s = Siec(self.input, self.hidden)
        s.w1 = self.combine2D(self.w1, other.w1)
        s.w2 = self.combine2D(self.w2, other.w2)
        s.b1 = self.combine1D(self.b1, other.b1)
        s.b2 = self.combine1D(self.b2, other.b2)
        return s

    def compute(self, arr):
        return np.tanh(np.tanh((arr.dot(self.w1)) + self.b1).dot(self.w2) + self.b2)

    def probability(self, arr1, arr2):
        # print(0.1 - np.linalg.norm(arr1 - arr2, ord=2) / math.pow(arr1.size, 0.5)/self.zakres)
        return max(0.1 - np.linalg.norm(arr1 - arr2, ord=2) / math.pow(arr1.size, 0.5) / self.zakres, 0.005)

    def combine2D(self, arr1, arr2):

        a = arr1.shape[0] * arr1.shape[1]
        b = np.random.randint(0, a)

        res = copy.deepcopy(arr1)

        for i in range(0, arr1.shape[0]):
            for j in range(0, arr1.shape[1]):
                if i + j * arr1.shape[0] >= b:
                    res[i][j] = arr2[i][j]
                if np.random.uniform(0, 1) < self.probability(arr1, arr2):
                    res[i][j] = np.random.uniform(-self.zakres, self.zakres)
                if np.random.uniform(0, 1) < 0.1:
                    res[i][j] += (np.random.uniform(-self.zakres, self.zakres) / 100)

        return res

    def combine1D(self, arr1, arr2):

        a = arr1.shape[0]
        b = np.random.randint(0, a)

        res = copy.deepcopy(arr1)

        for i in range(0, arr1.shape[0]):
            if i >= b:
                res[i] = arr2[i]
            if np.random.uniform(0, 1) < self.probability(arr1, arr2):
                res[i] = np.random.uniform(-self.zakres, self.zakres)
        return res

    def wypisz(self):
        print("w1: \n" + str(self.w1))
        print("w2: \n" + str(self.w2))
        print("b1: \n" + str(self.b1))
        print("b2: \n" + str(self.b2))


# @staticmethod
# def combine():


class driver:
    bonus = 0

    def __init__(self):
        self.N = Siec(8, 4)

    def makeChild(self, other):
        d = driver()
        d.N = self.N.offspring(other.N)
        return d

    def drive(self, c):

        S, R = c.S.d, c.R.d

        # print(S['track'])
        # print(S['speedX'])
        # print(S['speedY'])

        # divide = np.array([200, 200, 200, 200, 200, 200, 200, 250, 3.1415, 1])
        data = np.array(
            [S['track'][3], S['track'][6], S['track'][8], S['track'][9], S['track'][10], S['track'][12], S['track'][15],
             S['speedX']])

        data = data / 10

        result = self.N.compute(data)

        #print(S['lastLapTime'])

        # print(data)
        # print(result)
        # print(result)
        # print(result)

        R['accel'] = 0
        R['brake'] = 0
        R['steer'] = 0

        # if (S['track'][0] < 0): return

        if result[0] > 0: R['accel'] = abs(result[0])
        if result[0] < 0: R['brake'] = abs(result[0])
        R['steer'] = result[1]

        if S['rpm'] < 500 * (S['gear'] + 6):
            R['gear'] = max(S['gear'] - 1, 1)
        if S['rpm'] > 9000:
            R['gear'] = S['gear'] + 1

        # if result[0] < 0:
        #     print(data)
        #     print(result)

        return

    def getFitness(self, c):
        if math.isnan(c.S.d['distRaced']):
            return -1
        else:
            return c.S.d['distRaced']

    def Save(self, name):
        with open(name, 'wb') as p:
            pickle.dump(self.N, p)

    def Load(self, name):
        with open(name, 'rb') as p:
            loaded = pickle.load(p)
        self.N = loaded

    def kraksa(self, c):
        return c.S.d['track'][0] < 0

    def getBonus(self, c):
        return -abs(c.S.d['trackPos'] / 100) + (math.pow(1.008, c.S.d['speedX'] - 150) if c.S.d['speedX'] >= 150 else 0)


if __name__ == "__main__":
    os.system('torcs -nofuel -t 100000 &')
    C = snakeoil.Client()
    d = driver()

    if len(sys.argv) > 1:
        print(sys.argv[1])
        d.Load(sys.argv[1])

    d.N.wypisz()
    for step in range(0,1000000):
        C.get_servers_input()
        d.drive(C)
        C.respond_to_server()
    # print(d.getFitness(C))
    C.shutdown()
    os.system('pkill torcs')
