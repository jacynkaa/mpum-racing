#!/usr/bin/python
import snakeoil
import keyboard


def drive(c):
    S, R = c.S.d, c.R.d

    if keyboard.is_pressed('d'):
        R['steer'] -= 1 / (25 * (1 + 10*abs(R['steer'])))
    elif keyboard.is_pressed('a'):
        R['steer'] += 1 / (25 * (1 + 10*abs(R['steer'])))
    else:
        R['steer'] = 0

    R['accel'] = 0
    R['brake'] = 0

    if keyboard.is_pressed('w'):
        R['accel'] = 1

    # Automatic Transmission
    R['gear'] = 1
    if S['speedX'] > 50:
        R['gear'] = 2
    if S['speedX'] > 80:
        R['gear'] = 3
    if S['speedX'] > 110:
        R['gear'] = 4
    if S['speedX'] > 140:
        R['gear'] = 5
    if S['speedX'] > 170:
        R['gear'] = 6

    if keyboard.is_pressed('s'):
        R['brake'] = 1

        if S['speedX'] < 1:
            R['gear'] = -1
            R['accel'] = 1
            R['brake'] = 0

    # Traction Control System
    if ((S['wheelSpinVel'][2] + S['wheelSpinVel'][3]) -
            (S['wheelSpinVel'][0] + S['wheelSpinVel'][1]) > 5):
        R['accel'] -= .2

    return


if __name__ == "__main__":
    C = snakeoil.Client()
    for step in range(C.maxSteps, 0, -1):
        C.get_servers_input()
        drive(C)
        C.respond_to_server()
    C.shutdown()
