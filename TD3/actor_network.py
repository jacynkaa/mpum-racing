import numpy as np
import random

class ActorNetwork:

    def __init__(self, input_size, first_layer_size, second_layer_size, output_size, alpha):
        np.random.seed(int(1e9 * random.SystemRandom().random()))

        self.W_1 = np.random.normal(0, np.sqrt(2/(input_size + first_layer_size)), (input_size, first_layer_size))
        self.B_1 = np.zeros((first_layer_size, 1))

        self.W_2 = np.random.normal(0, np.sqrt(2/(first_layer_size + second_layer_size)), (first_layer_size, second_layer_size))
        self.B_2 = np.zeros((second_layer_size, 1))

        self.W_3 = np.random.normal(0, np.sqrt(2/(second_layer_size + output_size)), (second_layer_size, output_size))
        self.B_3 = np.zeros((output_size,1))

        self.alpha = alpha

        # NAG
        self.delta = 0.5 
        self.N_W_1 = np.zeros(self.W_1.shape) 
        self.N_W_2 = np.zeros(self.W_2.shape)
        self.N_W_3 = np.zeros(self.W_3.shape)

        self.N_B_1 = np.zeros(self.B_1.shape)
        self.N_B_2 = np.zeros(self.B_2.shape)
        self.N_B_3 = np.zeros(self.B_3.shape)
        #

        self.t_W_1 = np.zeros(self.W_1.shape) 
        self.t_W_2 = np.zeros(self.W_2.shape)
        self.t_W_3 = np.zeros(self.W_3.shape)

        self.t_B_1 = np.zeros(self.B_1.shape)
        self.t_B_2 = np.zeros(self.B_2.shape)
        self.t_B_3 = np.zeros(self.B_3.shape)


    def update(self, state, value):

        # NAG
        tmp_W_1 = self.W_1 - self.delta * self.N_W_1 
        tmp_W_2 = self.W_2 - self.delta * self.N_W_2 
        tmp_W_3 = self.W_3 - self.delta * self.N_W_3 
        
        tmp_B_1 = self.B_1 - self.delta * self.N_B_1 
        tmp_B_2 = self.B_2 - self.delta * self.N_B_2 
        tmp_B_3 = self.B_3 - self.delta * self.N_B_3 
        #

        a_0 = np.transpose(state)

        z_1 = np.transpose(np.transpose(a_0) @ tmp_W_1) + tmp_B_1 
        a_1 = np.maximum(0, z_1)

        z_2 = np.transpose(np.transpose(a_1) @ tmp_W_2) + tmp_B_2
        a_2 = np.maximum(0, z_2)

        z_3 = np.transpose(np.transpose(a_2) @ tmp_W_3) + tmp_B_3
        a_3 = z_3
        
        e_3 = -value * (1 - np.tanh(z_3) ** 2) 
        e_2 = (tmp_W_3 @ e_3) * ((z_2 > 0) * 1) 
        e_1 = (tmp_W_2 @ e_2) * ((z_1 > 0) * 1) 


        self.N_W_3 = (self.delta * self.N_W_3 + self.alpha * (a_2 @ np.transpose(e_3) / len(value)))
        self.W_3 -= self.N_W_3 
        self.N_B_3 = (self.delta * self.N_B_3 + self.alpha * np.reshape(np.mean(e_3, axis = 1), (self.B_3.shape[0], 1)))
        self.B_3 -= self.N_B_3 

        self.N_W_2 = (self.delta * self.N_W_2 + self.alpha * (a_1 @ np.transpose(e_2)) / len(value))
        self.W_2 -= self.N_W_2
        self.N_B_2 = (self.delta * self.N_B_2 + self.alpha * np.reshape(np.mean(e_2, axis = 1), (self.B_2.shape[0], 1)))
        self.B_2 -= self.N_B_2

        self.N_W_1 = (self.delta * self.N_W_1 + self.alpha * (a_0 @ np.transpose(e_1) / len(value)))
        self.W_1 -= self.N_W_1 
        self.N_B_1 = (self.delta * self.N_B_1 + self.alpha * np.reshape(np.mean(e_1, axis = 1), (self.B_1.shape[0], 1)))
        self.B_1 -= self.N_B_1 

    def result(self, example):
        example = np.transpose(example)
        a_1 = np.maximum(0, np.transpose(np.transpose(example) @ self.W_1) + self.B_1)
        a_2 = np.maximum(0, np.transpose(np.transpose(a_1) @ self.W_2) + self.B_2)
        a_3 = np.tanh(np.transpose(np.transpose(a_2) @ self.W_3) + self.B_3)
        return a_3

    def save(self):
        np.save('storage/actor/W_1', self.W_1)
        np.save('storage/actor/B_1', self.B_1)
        np.save('storage/actor/W_2', self.W_2)
        np.save('storage/actor/B_2', self.B_2)
        np.save('storage/actor/W_3', self.W_3)
        np.save('storage/actor/B_3', self.B_3)

    def load(self):
        self.W_1 = np.load('storage/actor/W_1.npy' )
        self.B_1 = np.load('storage/actor/B_1.npy' )
        self.W_2 = np.load('storage/actor/W_2.npy' )
        self.B_2 = np.load('storage/actor/B_2.npy' )
        self.W_3 = np.load('storage/actor/W_3.npy' )
        self.B_3 = np.load('storage/actor/B_3.npy' )

    def smooth_target_update(self, onlineNetwork, T):
        self.W_1 = (1 - T)*self.W_1 + T*onlineNetwork.W_1
        self.B_1 = (1 - T)*self.B_1 + T*onlineNetwork.B_1
        
        self.W_2 = (1 - T)*self.W_2 + T*onlineNetwork.W_2
        self.B_2 = (1 - T)*self.B_2 + T*onlineNetwork.B_2
        
        self.W_3 = (1 - T)*self.W_3 + T*onlineNetwork.W_3
        self.B_3 = (1 - T)*self.B_3 + T*onlineNetwork.B_3

