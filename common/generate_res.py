import numpy as np
import os
import sys
import keyboard
import snakeoil
import client
import time

tracks = ['wheel-2', 'cg-speedway', 'forza']
name = 'test'
debug=True

if __name__ == "__main__":
    os.system('rm wynik_'+name)
    os.system('touch wynik_'+name)
    os.system('pkill torcs')

    for track in tracks:
        filename = os.getcwd() + '/../common/quickrace-' + track + '.xml'
        print (filename)
        time.sleep(1)
        if  debug:  # sluzy do ogladania jazdy
            print("debug")
            os.system('cp ' +filename+' ~/.torcs/config/raceman/quickrace.xml ')
            os.system('torcs -nofuel -nodamage -nolaptime -t 10000000 &')
        else:
            os.system('torcs -r ' + filename + ' -nofuel -nodamage -nolaptime -t 10000000 &')

        C = snakeoil.Client()
        d = client.driver()

        if len(sys.argv) > 1:
            print(sys.argv[1])
            d.Load(sys.argv[1])

        for step in range(C.maxSteps, 0, -1):
            C.get_servers_input()
            d.drive(C)
            C.respond_to_server()
            if C.S.d['lastLapTime'] > 0.0:
                break
        if C.S.d['lastLapTime'] > 0.0:
            file = open('wynik_'+name, 'a')
            file.write(track + " " + name + ' ' + str(C.S.d['lastLapTime'])+'\n')
            file.close()

        C.shutdown()
        os.system('pkill torcs')

    # print(d.getFitness(C))
