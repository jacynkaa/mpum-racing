import snakeoil
import numpy as np
from collections.abc import Iterable
from collections import deque
from neural_network import QNetwork
from scipy.special import softmax

interestingFeatures = ('angle','fuel','gear','racePos','rpm','speedX','speedY','track','trackPos','wheelSpinVel','z','focus')

obs_size = 37
max_obs = np.ones(obs_size)
min_obs = np.zeros(obs_size)
qNetwork = QNetwork(obs_size, 100, 100, 9, 0.001)

def drive(c):
    global max_obs, min_obs    
    S, R= c.S.d, c.R.d

    ex = []
    for z in interestingFeatures:
        if isinstance(S[z], Iterable): ex.extend(S[z])
        else: ex.append(S[z])
    np_ex = np.array(ex)

    min_obs = np.minimum(min_obs, np_ex)
    max_obs = np.maximum(max_obs, np_ex)
    np_ex = ((np_ex - min_obs) / (max_obs - min_obs)) - 0.5

    results = qNetwork.result(np_ex)
    ind = np.argmax(results)

    R['brake'] = 0
    R['accel'] = 0
    R['steer'] = 0

    if ind == 0: 
        R['accel'] = 1
    elif ind == 1: 
        R['brake'] = 1
    elif ind == 3: 
        R['steer'] = -1
    elif ind == 4: 
        R['steer'] = 1 
    elif ind == 5:
        R['steer'] = 0.3
    elif ind == 6:
        R['steer'] = -0.3
    elif ind == 7:
        R['accel'] = 0.3
    elif ind == 8:
        R['brake'] = 0.3

    if S['rpm'] < 500 * (S['gear'] + 6):
        R['gear'] = max(S['gear'] - 1, 1)
    if S['rpm'] > 9000:
        R['gear'] = S['gear'] + 1

    return


if __name__ == "__main__":

    while True:

        qNetwork.load()
        min_obs = np.load('storage/min_obs.npy')
        max_obs = np.load('storage/max_obs.npy')
    
        restart = False
        C= snakeoil.Client()
        for step in range(C.maxSteps,0,-1):
            C.get_servers_input()
            drive(C)
            C.respond_to_server()

        C.shutdown()
