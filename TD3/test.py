import snakeoil
import numpy as np
import random
from collections.abc import Iterable
from actor_network import ActorNetwork

interestingFeatures = ('angle','fuel','gear','racePos','rpm','speedX','speedY','track','trackPos','wheelSpinVel','z','focus')

obs_size = 37
max_obs = np.ones(obs_size)
min_obs = np.zeros(obs_size)
actorNetwork = ActorNetwork(obs_size, 100, 100, 2, 0.001)

def drive(c):
    global restart, max_obs, min_obs    
    S, R= c.S.d, c.R.d

    ex = []
    for z in interestingFeatures:
        if isinstance(S[z], Iterable): ex.extend(S[z])
        else: ex.append(S[z])
    np_ex = np.array(ex)

    min_obs = np.minimum(min_obs, np_ex)
    max_obs = np.maximum(max_obs, np_ex)
    np_ex = ((np_ex - min_obs) / (max_obs - min_obs)) - 0.5

    action = actorNetwork.result(np_ex)

    accel = 0
    brake = 0
    if action[0] >= 0: accel = action[0]
    else : brake = -action[0]

    R['brake'] = brake
    R['accel'] = accel
    R['steer'] = action[1]


    # Automatic Transmission
    R['gear']=1
    if S['speedX']>50:
        R['gear']=2
    if S['speedX']>80:
        R['gear']=3
    if S['speedX']>110:
        R['gear']=4
    if S['speedX']>140:
        R['gear']=5
    if S['speedX']>170:
        R['gear']=6                

    if S['damage'] > 0:
        R['meta'] = 1
        restart = True
    return


if __name__ == "__main__":

    while True:
        actorNetwork.load()
        min_obs = np.load('storage/min_obs.npy')
        max_obs = np.load('storage/max_obs.npy')

        restart = False
        C= snakeoil.Client()
        for step in range(C.maxSteps,0,-1):
            C.get_servers_input()
            drive(C)
            C.respond_to_server()

        C.shutdown()
