import snakeoil
import numpy as np
import random
import copy
import generate_xml
import os
from collections.abc import Iterable
from collections import deque
from actor_network import ActorNetwork
from critic_network import CriticNetwork
from scipy.special import softmax

stuckTimeLimit = 4000
stuckTime = 0
minDist = 0.2
lastDist = 0
training = 0
restart = False

interestingFeatures = ('angle','fuel','gear','racePos','rpm','speedX','speedY','track','trackPos','wheelSpinVel','z','focus')

obs_size = 37
lastAction = np.zeros((1, 2))
lastObs = np.zeros((1, obs_size))
my_step = 0

max_obs = np.ones((1, obs_size))
min_obs = np.zeros((1, obs_size))
discount = 0.99

actorNetwork = ActorNetwork(obs_size, 80, 60, 2, 0.00001)
targetActorNetwork = copy.deepcopy(actorNetwork)

criticNetwork = CriticNetwork(obs_size + 2, 100, 100, 1, 0.001)
targetCriticNetwork = copy.deepcopy(criticNetwork)

rb_observation = deque()
rb_action = deque()
rb_reward = deque()
rb_nxt_observation = deque()


def drive(c):
    global stuckTime, lastDist, lastObs, restart, max_obs, min_obs, my_step, lastAction
    global training, step, targetActorNetwork, targetCriticNetwork
    global rb_observation, rb_action, rb_reward, rb_nxt_observation
    S, R= c.S.d, c.R.d

    my_step += 1
    targetCriticNetwork.smooth_target_update(criticNetwork, 0.0001)
    targetActorNetwork.smooth_target_update(actorNetwork, 0.0001)

    ex = []
    for z in interestingFeatures:
        if isinstance(S[z], Iterable): ex.extend(S[z])
        else: ex.append(S[z])
    np_ex = np.reshape(np.array(ex), (1, obs_size))

    # scale
    min_obs = np.minimum(min_obs, np_ex)
    max_obs = np.maximum(max_obs, np_ex)
    np_ex = ((np_ex - min_obs) / (max_obs - min_obs)) - 0.5

    # calculate reward from last action
    position_penalty = np.clip((S['trackPos'] * 1.1) ** 4, 0, 5)
    speed = (S['speedX'] * np.cos(S['angle']) - S['speedX'] * np.sin(S['angle']))
    speed_reward = np.sign(speed) * (abs(speed) ** (5/4))
    reward = (speed_reward - ((S['speedX']) * position_penalty))/1000

    print(S['distFromStart'], reward, my_step, position_penalty)

    # choose action
    action = actorNetwork.result(np_ex)

    # epsilon greedy exploration
    epsilon = max(1 - np.sqrt(my_step/2000000), 0.0001)
    if np.random.binomial(1, epsilon) == 1:
        action[0] = 2*(random.SystemRandom().random() - 0.5)
        action[1] = 2*(random.SystemRandom().random() - 0.5)

    accel = 0
    brake = 0
    if action[0] >= 0: accel = action[0]
    else : brake = -action[0]

    R['brake'] = brake
    R['accel'] = accel
    R['steer'] = action[1]

    if S['damage'] > 0:
        reward = -1
        np_ex = None

    # put non-terminal experience into replay buffer
    if np_ex is not None: 
        rb_observation.append(lastObs) 
        rb_action.append(lastAction)
        rb_reward.append(reward)
        rb_nxt_observation.append(np_ex)

    # bound on replay buffer length
    if len(rb_reward) > 250000:
        rb_observation.popleft()
        rb_action.popleft()
        rb_reward.popleft()
        rb_nxt_observation.popleft()

    # update with batch from replay buffer
    if len(rb_reward) > 100:
        batch_size = 32
        ex_to_update = [int(random.SystemRandom().random() * len(rb_reward)) for _ in range(batch_size)]

        observations = np.squeeze(np.array([rb_observation[i] for i in ex_to_update]))
        actions = np.squeeze(np.array([rb_action[i] for i in ex_to_update]))
        rewards = np.squeeze(np.array([rb_reward[i] for i in ex_to_update]))
        next_observations = np.squeeze(np.array([rb_nxt_observation[i] for i in ex_to_update]))

        # predict actions taken by actor 
        next_actions = np.transpose(targetActorNetwork.result(next_observations))

        # evaluate discounted reward for actions
        na_value_estimate = targetCriticNetwork.result(np.concatenate((next_actions, next_observations), axis = 1))

        # update critic
        t_update_values = rewards + (discount * na_value_estimate)
        criticNetwork.update(np.concatenate((actions, observations), axis = 1), t_update_values)

        # take gradient of function approximated with critic network with respect to action value
        gradient = criticNetwork.get_input_gradient(np.concatenate((actions, observations), axis = 1))
        action_gradient = gradient[:2]

        # perform gradient ascent
        actorNetwork.update(observations, action_gradient)
        


    # taken from snakeoil 
    # Automatic Transmission
    R['gear']=1
    if S['speedX']>50:
        R['gear']=2
    if S['speedX']>80:
        R['gear']=3
    if S['speedX']>110:
        R['gear']=4
    if S['speedX']>140:
        R['gear']=5
    if S['speedX']>170:
        R['gear']=6                
    #


    lastDist = S['distFromStart'] 
    lastObs = np_ex
    lastAction = np.reshape(action, (1, 2))

    if S['distFromStart'] - lastDist < minDist : stuckTime+=1
    else : stuckTime = 0

    # restart on damage or not moving for too long
    if stuckTime >= stuckTimeLimit or S['damage'] > 0:
        R['meta'] = 1
        stuckTime = 0
        lastObs = np.zeros((1, obs_size))
        lastAction = np.zeros((1, 2))
        restart = True
    return


if __name__ == "__main__":

    while True:

        if training == 0:
            criticNetwork.load()
            actorNetwork.load()
            targetCriticNetwork = copy.deepcopy(criticNetwork)
            targetActorNetwork = copy.deepcopy(actorNetwork)
            min_obs = np.load('storage/min_obs.npy')
            max_obs = np.load('storage/max_obs.npy')

        criticNetwork.save()
        actorNetwork.save()
        np.save('storage/min_obs', min_obs)
        np.save('storage/max_obs', max_obs)
        training += 1
        restart = False
        lastObs = np.zeros((1, obs_size))
        lastAction = np.zeros((1, 2))

        initial_distance = random.SystemRandom().choice(np.arange(3000))
        initial_speed = random.SystemRandom().choice(np.arange(10))
        track_name = random.SystemRandom().choice(('g-track-2', 'e-track-3', 'wheel-1', 'eroad', 'street-1'))

        print('initial distance:', initial_distance)
        print('track:', track_name)
        generate_xml.generate_map(3001, '/tmp/map.xml', initial_distance, initial_speed, track_name)
        os.system('torcs -r /tmp/map.xml -nofuel -nodamage -nolaptime -t 100000 &')

        C= snakeoil.Client()
        for step in range(C.maxSteps,0,-1):
            C.get_servers_input()
            drive(C)
            C.respond_to_server()

            if restart == True:
                os.system('pkill torcs')
                break

        C.shutdown()
