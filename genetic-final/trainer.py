import client
import os
import sys
import snakeoil
import functools
import math
from concurrent.futures import ThreadPoolExecutor
import concurrent
import threading
import subprocess
import generate_xml
import numpy as np
import random
import keyboard
import copy
import datetime

tracks = ['g-track-2', 'e-track-3', 'wheel-1', 'eroad', 'street-1', 'spring']
spring_discount = 1




def admin():
    return os.getuid() == 0


def multi(d, x, steps, initial_distance, initial_speed, track_name):
    if track_name == 'spring':
        steps = steps * spring_discount
    port = (3000 + x)

    generate_xml.generate_map(port, '/tmp/%d.xml' % x, initial_distance, initial_speed, track_name)

    # os.system('torcs -r /tmp/%d.xml -nofuel -nodamage -nolaptime -t 100000 &' % x)
    if x == 1 and keyboard.is_pressed('q') and keyboard.is_pressed('w'):
        print("jest")
        os.system('cp /tmp/%d.xml ~/.torcs/config/raceman/practice.xml' % x)
        os.system('torcs -nofuel -nodamage -nolaptime -t 10000000 &')
    else:
        os.system('torcs -r /tmp/%d.xml -nofuel -nodamage -nolaptime -t 10000000 &' % x)

    C = snakeoil.Client(p=port)
    res = 0
    for step in range(steps, 0, -1):
        C.get_servers_input()
        d.drive(C)
        C.respond_to_server()
        res += d.getBonus(C)
        if d.kraksa(C):  # w przypadku kraksy breakuje
            break
    res += d.getFitness(C)
    C.shutdown()
    if track_name == 'spring':
        res = res / spring_discount
    return res


def getNew(l, j, gen):
    l.sort(key=lambda x: -x[0])
    print(l)
    l[0][1].Save('gen/' + str(gen) + '_' + str(j) + '_fitness:' + str(int(l[0][0])))

    res = []

    r = int(math.sqrt(len(l)) + .2)

    for i in range(0, r):
        for j in range(0, r):
            if i != j:
                res.append(l[i][1].makeChild(l[j][1]))
            else:
                d = client.driver()
                d.N = l[i][1].N
                res.append(d)
    return res


def train(tribes, steps, gen):
    os.system('pkill torcs')
    l = [[] for i in range(0, len(tribes))]
    executor = ThreadPoolExecutor()

    for track in tracks:

        file = open('log', 'a')
        file.write(str(gen) + ": " + track + "time:" + str(datetime.datetime.now()) + "\n")
        file.close()

        initial_distance = np.random.randint(0, 200)
        initial_speed = random.uniform(0, 100)
        for i in range(0, len(tribes[0])):
            print(initial_speed)
            F = []
            for j in range(0, len(tribes)):
                F.append(executor.submit(multi, tribes[j][i], j + 1, steps, initial_distance, initial_speed, track))
            concurrent.futures.wait(F)
            for j in range(0, len(tribes)):
                if track == tracks[0]:
                    l[j].append([F[j].result(), tribes[j][i]])
                else:
                    l[j][i][0] += F[j].result()
            os.system('pkill torcs')

    for j in range(0, len(tribes)):
        tribes[j] = getNew(l[j], j, gen)
        
    if gen % 4 == 0:
        for j in range(0, len(tribes)):
            tribes[j].append(copy.deepcopy(tribes[(j + 1) % len(tribes)][0]))

    return tribes


if __name__ == "__main__":
    if not admin():
        print("brak uprawnien")
        exit(-1)

    size = 49
    tribes = [[client.driver() for i in range(0, size)] for j in range(0, 6)]

    if len(sys.argv) > 1:
        print(sys.argv[1])
        for t in tribes:
            for d in t:
                d.Load(sys.argv[1])

    for generation in range(0, 100):
        population = train(tribes, 5000, generation)
    exit(0)
