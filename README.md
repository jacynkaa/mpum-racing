Implementacja algorytmu genetic - genetic-final/


W folderze ddqn-client znajduje się implementacja agenta korzystającego z metody Double Deep Q-Learning.

train.py - program trenujący sieć

test.py - program testujący sieć, uruchamia jedynie klienta, aby przetestować sieć należy uruchomić torcs i rozpocząć wyścig wybierając jakokierowcę scr_server_1

storage - zawiera wytrenowaną sieć

DDQN-test_tracks.mp4 - nagranie przejazdu po trasach testowych


W folderach ddpg-client oraz TD3 znajdują się implementacje odpowiednich algorytmów o podobnej strukturze. Nie zawierają one wytrenowanych sieci ani filmów ponieważ trening przy ich pomocy nie dał dostatecznie dobrych wyników.
